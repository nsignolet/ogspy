<?php
/** $Id: admin.php 7596 2012-03-25 16:10:55Z ninety $ **/
/**
 * Fonctions d'administrations
 * @package OGSpy
 * @version 3.04b ($Rev: 7596 $)
 * @subpackage admin
 * @author Kyser
 * @created 16/12/2005
 * @copyright Copyright &copy; 2007, http://ogsteam.fr/
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

if (!defined('IN_SPYOGAME')) {
    die("Hacking attempt");
}
// Verification des droits admins
if ($user_data["user_admin"] != 1 && $user_data["user_coadmin"] != 1 && $user_data["management_user"] != 1) {
    redirection("index.php?action=message&amp;id_message=forbidden&amp;info");
}

?>

<div class="row">
    <div class="col-md-12 text-center">
        <div class="btn-group btnGroupAdmin" role="group">
            <?php
                if (!isset($pub_subaction)) {
                    if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1) $pub_subaction = "infoserver";
                    else $pub_subaction = "member";
                }

                if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1) {
                    if ($pub_subaction != "infoserver") {
                        ?> <button type="button" class="btn btn-primary" onclick="window.location ='index.php?action=administration&amp;subaction=infoserver';"><?= $lang['ADMIN_TITLE_GENERAL_INFO'] ?></button> <?php
                    } else {
                        ?> <button type="button" class="btn btn-primary disabled"><?= $lang['ADMIN_TITLE_GENERAL_INFO'] ?></button> <?php
                    }
                }

                if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1) {
                    if ($pub_subaction != "parameter") {
                        ?> <button type="button" class="btn btn-primary" onclick="window.location = 'index.php?action=administration&amp;subaction=parameter';"><?= $lang['ADMIN_TITLE_SERVER_CONF'] ?></button> <?php
                    } else {
                        ?> <button type="button" class="btn btn-primary disabled"><?= $lang['ADMIN_TITLE_SERVER_CONF'] ?></button> <?php
                    }
                }

                if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1) {
                    if ($pub_subaction != "affichage") {
                        ?> <button type="button" class="btn btn-primary" onclick="window.location = 'index.php?action=administration&amp;subaction=affichage';"><?= $lang['ADMIN_TITLE_DISPLAY_CONF'] ?></button> <?php
                    } else {
                        ?> <button type="button" class="btn btn-primary disabled"><?= $lang['ADMIN_TITLE_DISPLAY_CONF'] ?></button> <?php
                    }
                }

                if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1 || $user_data["management_user"] == 1) {
                    if ($pub_subaction != "member") {
                        ?> <button type="button" class="btn btn-primary" onclick="window.location = 'index.php?action=administration&amp;subaction=member';"><?= $lang['ADMIN_TITLE_MEMBER_CONF'] ?></button> <?php
                    } else {
                        ?> <button type="button" class="btn btn-primary disabled"><?= $lang['ADMIN_TITLE_MEMBER_CONF'] ?></button> <?php
                    }
                }

                if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1 || $user_data["management_user"] == 1) {
                    if ($pub_subaction != "group") {
                        ?> <button type="button" class="btn btn-primary" onclick="window.location = 'index.php?action=administration&amp;subaction=group';"><?= $lang['ADMIN_TITLE_GROUP_CONF'] ?></button> <?php
                    } else {
                        ?> <button type="button" class="btn btn-primary disabled"><?= $lang['ADMIN_TITLE_GROUP_CONF'] ?></button> <?php
                    }
                }

                if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1) {
                    if ($pub_subaction != "viewer") {
                        ?> <button type="button" class="btn btn-primary" onclick="window.location = 'index.php?action=administration&amp;subaction=viewer';"><?= $lang['ADMIN_TITLE_LOGS_CONF'] ?></button> <?php
                    } else {
                        ?> <button type="button" class="btn btn-secondary disabled"><?= $lang['ADMIN_TITLE_LOGS_CONF'] ?></button> <?php
                    }
                }

                if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1) {
                    if ($pub_subaction != "mod") {
                        ?> <button type="button" class="btn btn-primary" onclick="window.location = 'index.php?action=administration&amp;subaction=mod';"><?= $lang['ADMIN_TITLE_MODS_CONF'] ?></button> <?php
                    } else {
                        ?> <button type="button" class="btn btn-primary disabled"><?= $lang['ADMIN_TITLE_MODS_CONF'] ?></button> <?php
                    }
                }
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
                    <?php
                        switch ($pub_subaction) {
                            case "member" :
                                require_once("admin_members.php");
                                break;

                            case "group" :
                                require_once("admin_members_group.php");
                                break;

                            case "parameter" :
                                require_once("admin_parameters.php");
                                break;

                            case "affichage" :
                                require_once("admin_affichage.php");
                                break;

                            case "viewer" :
                                require_once("admin_viewer.php");
                                break;

                            case "mod" :
                                require_once("admin_mod.php");
                                break;

                            default:
                                require_once("admin_infoserver.php");
                                break;
                        }
                    ?>
    </div>
</div>
