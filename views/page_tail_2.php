<?php
/**
 * HTML Footer Light
 * @package OGSpy
 * @version 3.04b ($Rev: 7508 $)
 * @subpackage views
 * @author Kyser
 * @created 15/12/2005
 * @copyright Copyright &copy; 2007, http://ogsteam.fr/
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

if (!defined('IN_SPYOGAME')) {
    die("Hacking attempt");
}

$php_end = benchmark();
$php_timing = $php_end - $php_start - $sql_timing;
$db->sql_close(); // fermeture de la connexion à la base de données

?>

</td>
</tr>

<?php

global $ogspy_phperror;

if (is_array($ogspy_phperror) && count($ogspy_phperror)) {
    echo "\n<tr>\n\t<td><table><tr><th>".$lang['FOOTER_PHPERRORS']."</th></tr>";

    foreach ($ogspy_phperror as $line) {
        echo "\n<tr><td>$line</td></tr>";
    }

    echo "</table>\n\t</td>\n</tr>";
}

?>


</table>


<!--===============================================================================================-->
<script src="./skin/KaoSkin/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="./skin/KaoSkin/vendor/bootstrap/js/popper.js"></script>
<script src="./skin/KaoSkin/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="./skin/KaoSkin/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="./skin/KaoSkin/vendor/tilt/tilt.jquery.min.js"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="./skin/KaoSkin/js/main.js"></script>

</body>
</html>
