<?php
/**
 * Panneau administration des options Modules
 * @package OGSpy
 * @version 3.04b ($Rev: 7508 $)
 * @subpackage views
 * @author Aeris
 * @created 07/04/2007
 * @copyright Copyright &copy; 2007, http://ogsteam.fr/
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

if (!defined('IN_SPYOGAME')) die("Hacking attempt");

$mod_list = mod_list();
?>
<?php echo($lang['ADMIN_MOD_LIST']); ?>
<table  class="table tableMods">
    <tr>
        <thead>
            <th colspan="5"><?php echo($lang['ADMIN_MOD_USER']); ?></th>
            <th><?php echo($lang['ADMIN_MOD_MENUVIEW']); ?></th>
        </thead>
    </tr>
    <?php
    $mods = $mod_list["actived"];
    while ($mod = current($mods)) {
        if ($mod["admin_only"] == 0) {
            echo "\t" . "<tr>";
            echo "<td width='200'>" . $mod["title"] . " (" . $mod["version"] . ")</td>";
            echo "<td width='50'><a href='index.php?action=mod_up&amp;mod_id=" . $mod['id'] . "'><img src='images/asc.png' title='Monter'></a>&nbsp;<a href='index.php?action=mod_down&amp;mod_id=" . $mod['id'] . "'><img src='images/desc.png' title='Descendre'></a></td>";
            echo "<td width='100'><a href='index.php?action=mod_disable&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_DISABLE']."</a></td>";
            echo "<td width='100'><a href='index.php?action=mod_uninstall&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_REMOVE']."</a></td>";
            echo "<td width='100'>";
            if (!$mod["up_to_date"]) {
                echo "<a href='index.php?action=mod_update&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_UPDATE']."</a>";
            }
            echo "</td>";
            echo "<td width='100'><a href='index.php?action=mod_admin&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_NORMAL']."</a></td>";
            echo "</tr>";
            echo "\n";


        }
        next($mods);
    }
    echo "<tr>
            <thead>
                <th colspan=\"5\">".$lang['ADMIN_MOD_ADMIN']."</th>
                <th>".$lang['ADMIN_MOD_MENUVIEW']."</th>
            </thead>
        </tr>";
    $mods = $mod_list["actived"];
    while ($mod = current($mods)) {
        if ($mod["admin_only"] == 1) {
            echo "\t" . "<tr>";
            echo "<td width='200'>" . $mod["title"] . " (" . $mod["version"] . ")</td>";
            echo "<td width='50'><a href='index.php?action=mod_up&amp;mod_id=" . $mod['id'] . "'><img src='images/asc.png' title='Monter'></a>&nbsp;<a href='index.php?action=mod_down&amp;mod_id=" . $mod['id'] . "'><img src='images/desc.png' title='Descendre'></a></td>";
            echo "<td width='100'><a href='index.php?action=mod_disable&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_DISABLE']."</a></td>";
            echo "<td width='100'><a href='index.php?action=mod_uninstall&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_REMOVE']."</a></td>";
            echo "<td width='100'>";
            if (!$mod["up_to_date"]) {
                echo "<a href='index.php?action=mod_update&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_UPDATE']."</a>";
            }
            echo "</td>";
            echo "<td width='100'><a href='index.php?action=mod_normal&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_ADMIN']."</a></td>";
            echo "</tr>";
            echo "\n";


        }
        next($mods);
    }
    ?>
</table>
<?php echo($lang['ADMIN_MOD_LIST_INACTIVE']); ?>
<table  class="table tableMods">
    <?php
    $mods = $mod_list["disabled"];
    while ($mod = current($mods)) {
        echo "\t" . "<tr>";
        echo "<td width='250' colspan='2'>" . $mod["title"] . " (" . $mod["version"] . ")</td>";
        echo "<td width='100'><a href='index.php?action=mod_active&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_ENABLE']."</a></td>";
        echo "<td width='100'><a href='index.php?action=mod_uninstall&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_REMOVE']."</a></td>";
        if (!$mod["up_to_date"]) {
            echo "<td width='100'><a href='index.php?action=mod_update&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_UPDATE']."</a></td>";
        } else echo "<td width='100'>&nbsp;</td>";
        echo "</tr>";
        echo "\n";

        next($mods);
    }
    ?>
</table>
<?php echo($lang['ADMIN_MOD_NOT_INSTALLED']); ?>
<table  class="table tableMods">
    <?php
    $mods = $mod_list["install"];
    while ($mod = current($mods)) {
        echo "\t" . "<tr>";
        echo "<td width='200'>" . $mod["title"] . "</td>";
        echo "<td width='300' colspan='5'><a href='index.php?action=mod_install&amp;directory=" . $mod['directory'] . "'>".$lang['ADMIN_MOD_INSTALL']."</a></td>";
        echo "</tr>";

        next($mods);
    }
    ?>
</table>
    <?php
    $mods = $mod_list["wrong"];
    if(!empty($mods)) {
        echo($lang['ADMIN_MOD_INVALID']); ?>
        <table  class="table tableMods">
            <?php
            while ($mod = current($mods)) {
                echo "\t" . "<tr>";
                    echo "<td width='200'>" . $mod["title"] . "</td>";
                    echo "<td width='300' colspan='5'><a href='index.php?action=mod_uninstall&amp;mod_id=" . $mod['id'] . "'>".$lang['ADMIN_MOD_REMOVE']."</a></td>";
                    echo "</tr>";
                echo "\n";

                next($mods);
            }
            ?>
        </table>
<?php
    }
?>