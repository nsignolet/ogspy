<?php
/**
 * Panneau d'Administration : paramètres et options du serveur
 * @package OGSpy
 * @version 3.04b ($Rev: 7508 $)
 * @subpackage views
 * @author Kyser
 * @created 15/12/2005
 * @copyright Copyright &copy; 2007, http://ogsteam.fr/
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

if (!defined('IN_SPYOGAME')) {
    die("Hacking attempt");
}

if ($user_data["user_admin"] != 1 && $user_data["user_coadmin"] != 1) {
    redirection("index.php?action=message&amp;id_message=forbidden&amp;info");
}

$max_battlereport = $server_config['max_battlereport'];
$max_favorites = $server_config['max_favorites'];
$max_spyreport = $server_config['max_spyreport'];
$server_active = $server_config['server_active'] == 1 ? "checked" : "";
$session_time = $server_config['session_time'];
$max_keeplog = $server_config['max_keeplog'];
$debug_log = $server_config['debug_log'] == 1 ? "checked" : "";
$log_phperror = $server_config['log_phperror'] == 1 ? "checked" : "";
$reason = $server_config['reason'];
$ally_protection = $server_config['ally_protection'];
$allied = $server_config['allied'];
$url_forum = $server_config['url_forum'];
$max_keeprank = $server_config['max_keeprank'];
$keeprank_criterion = $server_config['keeprank_criterion'];
$max_keepspyreport = $server_config['max_keepspyreport'];
$servername = $server_config['servername'];
$max_favorites_spy = $server_config['max_favorites_spy'];
$disable_ip_check = $server_config['disable_ip_check'] == 1 ? "checked" : "";
$num_of_galaxies = (isset ($pub_num_of_galaxies)) ? $pub_num_of_galaxies : $server_config['num_of_galaxies'];
$num_of_systems = (isset ($pub_num_of_systems)) ? $pub_num_of_systems : $server_config['num_of_systems'];
$block_ratio = $server_config['block_ratio'] == 1 ? "checked" : "";
$ratio_limit = $server_config['ratio_limit'];
$speed_uni = $server_config['speed_uni'];
$ddr = $server_config['ddr'];
$astro_strict = $server_config['astro_strict'];
$config_cache = $server_config['config_cache'];
$mod_cache = $server_config['mod_cache'];
?>

<form method="POST" action="index.php" class="formAdminParameter">
    <h2><?php echo($lang['ADMIN_PARAMS_GENERAL']); ?></h2>
    <input type="hidden" name="action" value="set_serverconfig">
    <input name="max_battlereport" type="hidden" size="5" value="10">
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_SERVERNAME']); ?></label>
        <div class="col-sm-5">
            <input type="text" class="form-control form-control" name="servername" value="<?php echo $servername; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_ACTIVATESERVER']); ?><?php echo help("admin_server_status");?></label>
        <div class="col-sm-5">
            <input name="server_active" class="form-control form-control" type="checkbox" value="1" <?php echo $server_active;?>>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_OFFREASON']); ?><?php echo help("admin_server_status_message");?></label>
        <div class="col-sm-5">
            <input type="text" class="form-control form-control" name="reason" value="<?php echo $reason; ?>">
        </div>
    </div>
    <h2><?php echo($lang['ADMIN_PARAMS_MEMBEROPTIONS']); ?></h2>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_ALLOW_IPCHECKDISABLING']); ?><?php echo help("admin_check_ip");?></label>
        <div class="col-sm-5">
            <input name="disable_ip_check" type="checkbox" class="form-control form-control" value="1" <?php echo $disable_ip_check;?>>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_MAXSSFAVORITES']); ?></label>
        <div class="col-sm-5">
            <input name="max_favorites" class="form-control form-control" type="text" size="5" maxlength="2" value="<?php echo $max_favorites; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_MAXREFAVORITES']); ?></label>
        <div class="col-sm-5">
            <input name="max_favorites_spy" class="form-control form-control" type="text" size="5" maxlength="2" value="<?php echo $max_favorites_spy; ?>">
        </div>
    </div>
    <h2><?php echo($lang['ADMIN_PARAMS_SESSIONS_TITLE']); ?></h2>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo $lang['ADMIN_PARAMS_SESSIONS_DURATION']; echo help("admin_session_infini");?></label>
        <div class="col-sm-5">
            <input name="session_time" class="form-control form-control" type="text" maxlength="3" value="<?php echo $session_time; ?>">
        </div>
    </div>
    <h2><?php echo($lang['ADMIN_PARAMS_ALLYPROTECT']); ?></h2>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_ALLYPROTECTLIST']); ?>
        <br/><i><?php echo($lang['ADMIN_PARAMS_ALLYPROTECTNOTICE']); ?></i></label>
        <div class="col-sm-5">
            <input type="text" class="form-control form-control" name="ally_protection" value="<?php echo $ally_protection; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_ALLYPROTECTFRIENDS']); ?>
            <br/><i><?php echo($lang['ADMIN_PARAMS_ALLYPROTECTNOTICE']); ?></i></label>
        <div class="col-sm-5">
            <input type="text" class="form-control form-control" name="allied" value="<?php echo $allied; ?>">
        </div>
    </div>
    <h2><?php echo($lang['ADMIN_PARAMS_OTHER']); ?></h2>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_FORUMLINK']); ?></label>
        <div class="col-sm-5">
            <input type="text" class="form-control form-control" name="url_forum" value="<?php echo $url_forum; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_DEBUGSQL']); ?><?php echo help("admin_save_transaction");?>
        <br/><i><?php echo($lang['ADMIN_PARAMS_DEBUGSQLALERT']); ?></i></label>
        <div class="col-sm-5">
            <input name="debug_log" class="form-control form-control" type="checkbox" value="1" <?php echo $debug_log;?>>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_RATIOMOD']); ?></label>
        <div class="col-sm-5">
            <input name="block_ratio" class="form-control form-control" type="checkbox" value="1" <?php echo $block_ratio;?>>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_RATIOBLIMIT']); ?></label>
        <div class="col-sm-5">
            <input name="ratio_limit" class="form-control form-control" type="text" size="10" maxlength="9" value="<?php echo $ratio_limit; ?>">
        </div>
    </div>
    <h2><?php echo($lang['ADMIN_PARAMS_SERVICE']); ?></h2>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_DURATION_RANKS']); ?></label>
        <div class="col-sm-5 form-row">
            <div class="col-sm-6 no-padding">
                <input type="text" class="form-control form-control" name="max_keeprank" maxlength="4" value="<?php echo $max_keeprank; ?>">&nbsp;
            </div>
            <div class="col-sm-6 no-padding">
                <select name="keeprank_criterion" class="form-control form-control">
                    <option value="quantity" <?php echo $keeprank_criterion == "quantity" ? "selected" : "";?>><?php echo($lang['ADMIN_PARAMS_DURATION_NUMBER']); ?></option>
                    <option value="day" <?php echo $keeprank_criterion == "day" ? "selected" : "";?>><?php echo($lang['ADMIN_PARAMS_DURATION_DAYS']); ?></option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_MAX_SPYREPORTS']); ?></label>
        <div class="col-sm-5">
            <input type="text" class="form-control form-control" name="max_spyreport" maxlength="4" size="5" value="<?php echo $max_spyreport; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_DURATION_SPYREPORTS']); ?></label>
        <div class="col-sm-5">
            <input type="text" class="form-control form-control" name="max_keepspyreport" maxlength="4" size="5" value="<?php echo $max_keepspyreport; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_DURATION_LOGS']); ?></label>
        <div class="col-sm-5">
            <input name="max_keeplog" class="form-control form-control" type="text" size="5" maxlength="3" value="<?php echo $max_keeplog; ?>">
        </div>
    </div>
    <?php
    if ($user_data["user_admin"] == 1) {
        ?>
        <h2><?php echo($lang['ADMIN_PARAMS_GAME_OPTIONS']); ?></h2>
        <div class="form-group row">
            <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_GAME_GALAXIES']); ?><?php echo help("profile_galaxy");?></label>
            <div class="col-sm-5 form-row">
                <div class="col-sm-10 no-padding">
                    <input name="num_of_galaxies" class="form-control form-control" class="form-control form-control" id="galaxies" type="text" maxlength="3"
                           value="<?php echo $num_of_galaxies; ?>"
                           onChange="if (!confirm('<?php echo($lang['ADMIN_PARAMS_GAME_GALAXIES_POPUP']); ?>')){document.getElementById('galaxies').value='<?php echo $num_of_galaxies; ?>';}"
                           readonly="readonly">
                </div>
                <div class="col-sm-2 no-padding">
                    <input name="enable_input_num_galaxies" class="form-control form-control"
                            type="checkbox"
                            onClick="(this.checked)? document.getElementById('galaxies').readOnly=false : document.getElementById('galaxies').readOnly=true;">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_GAME_SYSTEMS']); ?><?php echo help("profile_galaxy");?></label>
            <div class="col-sm-5 form-row">
                <div class="col-sm-10 no-padding">
                    <input name="num_of_systems" class="form-control form-control" id="systems" type="text" size="5" maxlength="3"
                           value="<?php echo $num_of_systems; ?>"
                           onChange="if (!confirm('<?php echo($lang['ADMIN_PARAMS_GAME_SYSTEMS_POPUP']); ?>')){document.getElementById('systems').value='<?php echo $num_of_systems; ?>';}"
                           readonly="readonly">
                </div>
                <div class="col-sm-2 no-padding">
                    <input name="enable_input_num_systems" class="form-control form-control"
                           type="checkbox"
                           onClick="(this.checked)? document.getElementById('systems').readOnly=false : document.getElementById('systems').readOnly=true;">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_GAME_SPEED']); ?><?php echo help("profile_speed_uni");?></label>
            <div class="col-sm-5 form-row">
                <div class="col-sm-10 no-padding">
                    <input name="speed_uni" class="form-control form-control" id="speed_uni" type="text" size="5" maxlength="2"
                           value="<?php echo $speed_uni; ?>"
                           onChange="if (!confirm('<?php echo($lang['ADMIN_PARAMS_GAME_SPEED_POPUP']); ?>\n')){document.getElementById('speed_uni').value='<?php echo $speed_uni; ?>';}"
                           readonly="readonly">
                </div>
                <div class="col-sm-2 no-padding">
                    <input name="enable_input_speed_uni" class="form-control form-control"
                           type="checkbox"
                           onClick="(this.checked)? document.getElementById('speed_uni').readOnly=false : document.getElementById('speed_uni').readOnly=true;">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_GAME_DDR']); ?><?php echo help("profile_ddr");?></label>
            <div class="col-sm-5">
                <input name="ddr" class="form-control form-control" value="1" type="checkbox"<?php print ($ddr == 1) ? ' checked' : '' ?>>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_GAME_ASTRO']); ?><?php echo help("astro_strict");?></label>
            <div class="col-sm-5">
                <input name="astro_strict" value="1" class="form-control form-control" type="checkbox"<?php print ($astro_strict == 1) ? ' checked' : '' ?>>
            </div>
        </div>
        <?php
    }
    ?>
    <h2><?php echo($lang['ADMIN_PARAMS_CACHE']); ?></h2>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_CACHE_RESET']); ?></label>
        <div class="col-sm-5">
            <input name="regenere_cache" class="form-control form-control" type="checkbox" value="0"/>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_CACHE_DURATION_CONFIG']); ?> <?php echo help("config_cache");?>
            <br/><i><?php echo($lang['ADMIN_PARAMS_CACHE_DURATION_NOTICE']); ?></i></label>
        <div class="col-sm-5">
            <input type="text" class="form-control form-control" name="config_cache" maxlength="10" size="10" value="<?php echo $config_cache; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_CACHE_DURATION_MOD']); ?><?php echo help("mod_cache"); ?></label>
        <div class="col-sm-5">
            <input type="text" class="form-control form-control" name="mod_cache" maxlength="10" size="10" value="<?php echo $mod_cache; ?>">
        </div>
    </div>
    <h2><?php echo($lang['ADMIN_PARAMS_DEBUG']); ?></h2>
    <div class="form-group row">
        <label class="col-sm-7 col-form-label col-form-label"><?php echo($lang['ADMIN_PARAMS_DEBUG_PHP']); ?>
        <br/><i><?php echo($lang['ADMIN_PARAMS_DEBUG_PHP_NOTICE']); ?></i></label>
        <div class="col-sm-5">
            <input name="log_phperror" class="form-control form-control" type="checkbox" value="1" <?php echo $log_phperror;?>>
        </div>
    </div>
    <?php
    if ($user_data["user_admin"] == 1) {
        ?>
        <h2><?php echo($lang['ADMIN_PARAMS_GOOGLE_CLOUD']); ?></h2>
        <i><?php echo($lang['ADMIN_PARAMS_GOOGLE_NOTIF']); ?></i>
        <div class="col-sm-12">
            <?php require_once 'gcm_users.php';?>
        </div>
        <?php
    }
    ?>
    <div class="form-group row">
        <div class="col-sm-12 text-center">
            <br/>
            <div class="col-sm-1"></div>
            <input type="submit" class="btn btn-primary btn-lg col-sm-5" value="<?php echo($lang['ADMIN_PARAMS_VALIDATE']); ?>">&nbsp;<input type="reset" class="btn btn-warning btn-lg col-sm-5" value="<?php echo($lang['ADMIN_PARAMS_CANCEL']); ?>">
            <div class="col-sm-1"></div>
        </div>
    </div>
    <br/>
</form>
