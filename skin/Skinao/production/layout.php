<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <meta name="language" content="<?php echo($lang['HEAD_LANGUAGE']); ?>">
    <title><?php echo $server_config["servername"] . " - OGSpy " . $server_config["version"];?></title>

    <!-- Bootstrap -->
    <link href="skin/Skinao/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="skin/Skinao/build/css/custom.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="skin/Skinao/build/css/skinao.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><img src="skin/Skinao/build/images/logo.png" width="120px" alt="OGSpy"></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_info">
                <h2>Univers - Player</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                    <?php
                    if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1 || $user_data["management_user"] == 1) {
                        echo "<li><a href='index.php?action=administration'>".$lang['MENU_ADMIN']."</a></li>";
                    }
                    ?>
                    <li><a href='index.php?action=profile'><?php echo($lang['MENU_PROFILE']); ?></a></li>
                    <li><a href='index.php?action=home'><?php echo($lang['MENU_HOME']); ?></a></li>
                    <li><a href='index.php?action=galaxy'><?php echo($lang['MENU_GALAXY']); ?></a></li>
                    <li><a href='index.php?action=cartography'><?php echo($lang['MENU_ALLIANCES']); ?></a></li>
                    <li><a href='index.php?action=search'><?php echo($lang['MENU_RESEARCH']); ?></a></li>
                    <li><a href='index.php?action=ranking'><?php echo($lang['MENU_RANKINGS']); ?></a></li>
                    <li><a href='index.php?action=statistic'><?php echo($lang['MENU_UPDATE_STATUS']); ?></a></li>

                    <li><a href="#"><i class="fas fa-edit"></i> <?php echo($lang['MENU_MODULES']); ?><span class="fas fa-chevron-down right"></span></a>
                        <ul class="nav child_menu">
                            <?php
                                 $request = "SELECT action, menu FROM " . TABLE_MOD . " WHERE active = 1 AND `admin_only` = '0' order by position, title";
                                 $result = $db->sql_query($request);

                                 if ($db->sql_numrows($result)) {
                                     while ($val = $db->sql_fetch_assoc($result)) {
                                         echo '<li><a href="index.php?action=' . $val['action'] . '">' . $val['menu'] . '</a></li>';
                                     }
                                 }

                                if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1) {
                                    $request = "SELECT action, menu FROM " . TABLE_MOD . " WHERE active = 1 and `admin_only` = '1' order by position, title";
                                    $result = $db->sql_query($request);

                                    if ($db->sql_numrows($result)) {
                                        while ($val = $db->sql_fetch_assoc($result)) {
                                            echo '<li><a href="index.php?action=' . $val['action'] . '">' . $val['menu'] . '</a></li>';
                                        }
                                    }
                                }

                            ?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <?php
                        if ($server_config["url_forum"] != "") {
                            echo "<li><a href='" . $server_config["url_forum"] . "'>".$lang['MENU_FORUM']."</a></li>";
                        }
                    ?>
                    <li><a href="index.php?action=about"><?php echo($lang['MENU_ABOUT']); ?></a></li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="index.php?action=logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
                    <script type="text/javascript">
                var date = new Date;
                var delta = Math.round((<?php echo (time() * 1000);?> -date.getTime()) / 1000);
                function Timer() {
                    var days = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"];
                    var months = ["Jan", "Fév", "Mar", "Avr", "Mai", "Jui", "Jui", "Aoû", "Sep", "oct", "nov", "déc"];

                    date = new Date;
                    date.setTime(date.getTime() + delta * 1000);
                    var hour = date.getHours();
                    var min = date.getMinutes();
                    var sec = date.getSeconds();
                    var day = days[date.getDay()];
                    var day_number = date.getDate();
                    var month = months[date.getMonth()];
                    if (sec < 10) sec = "0" + sec;
                    if (min < 10) min = "0" + min;
                    if (hour < 10) hour = "0" + hour;

                    var datetime = day + " " + day_number + " " + month + " " + hour + ":" + min + ":" + sec;

                    if (document.getElementById) {
                        document.getElementById("datetime").innerHTML = datetime;
                    }
                }

                go_visibility = [];
                function goblink() {
                    if (document.getElementById && document.all) {
                        var blink_tab = document.getElementsByTagName('blink');
                        for (var a = 0; a < blink_tab.length; a++) {
                            if (go_visibility[a] != "visible")
                                go_visibility[a] = "visible";
                            else
                                go_visibility[a] = "hidden";
                            blink_tab[a].style.visibility = go_visibility[a];
                        }
                    }
                }

                function Biper() {
                    Timer();
                    goblink();

                    setTimeout("Biper()", 1000);
                }

                window.onload = Biper;
            </script>
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fas fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false" title="<?php echo($lang['MENU_SERVER_TIME']); ?>">
                    <i class="fas fa-clock"></i> <span id="datetime"><?php echo($lang['MENU_WAITING']); ?></span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <?= $content ?>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            <?= "OGSpy " . $server_config["version"];?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="skin/Skinao/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="skin/Skinao/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="skin/Skinao/build/js/custom.min.js"></script>
	
<?php
  $php_end = benchmark();
  $php_timing = $php_end - $php_start - $sql_timing;
  $nb_requete = $db->nb_requete;
  $nb_users = user_get_nb_active_users();
  $db->sql_close(); // fermeture de la connexion à la base de données
?>

  </body>
</html>
