<?php

define('OGSPY_MOD_LOADED', 1);

$cache_mod = array (
  'api' => 
  array (
    'action' => 'api',
    'menu' => 'api',
    'root' => 'api',
    'link' => 'api.php',
    'admin_only' => '1',
  ),
  'timeobservatory' => 
  array (
    'action' => 'timeobservatory',
    'menu' => 'timeobservatory',
    'root' => 'timeobservatory',
    'link' => 'timeobservatory.php',
    'admin_only' => '0',
  ),
  'recycleurs' => 
  array (
    'action' => 'recycleurs',
    'menu' => 'Recycleurs',
    'root' => 'recycleurs',
    'link' => 'index.php',
    'admin_only' => '0',
  ),
  'flottes' => 
  array (
    'action' => 'flottes',
    'menu' => 'Flottes',
    'root' => 'flottes',
    'link' => 'flottes.php',
    'admin_only' => '0',
  ),
  'autoupdate' => 
  array (
    'action' => 'autoupdate',
    'menu' => 'AutoUpdate',
    'root' => 'autoupdate',
    'link' => 'autoupdate.php',
    'admin_only' => '1',
  ),
  'xtense' => 
  array (
    'action' => 'xtense',
    'menu' => '<span onclick="window.open(this.parentNode.href, \'Xtense\', \'width=750, height=550, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no\'); return false;">Xtense</span>',
    'root' => 'xtense',
    'link' => 'index.php',
    'admin_only' => '1',
  ),
  'superapix' => 
  array (
    'action' => 'superapix',
    'menu' => 'superapix',
    'root' => 'superapix',
    'link' => 'index.php',
    'admin_only' => '1',
  ),
  'attaques' => 
  array (
    'action' => 'attaques',
    'menu' => 'Gestion des attaques',
    'root' => 'attaques',
    'link' => 'index.php',
    'admin_only' => '0',
  ),
  'hofrc' => 
  array (
    'action' => 'hofrc',
    'menu' => 'Hall Of Fame RC',
    'root' => 'hofrc',
    'link' => 'index.php',
    'admin_only' => '0',
  ),
  'cdr' => 
  array (
    'action' => 'cdr',
    'menu' => 'Champs de ruines',
    'root' => 'cdr',
    'link' => 'cdr.php',
    'admin_only' => '0',
  ),
  'eXpedition' => 
  array (
    'action' => 'eXpedition',
    'menu' => 'eXpedition',
    'root' => 'expedition',
    'link' => 'index.php',
    'admin_only' => '0',
  ),
  'bt_hof' => 
  array (
    'action' => 'bt_hof',
    'menu' => 'Build.Tech HOF',
    'root' => 'bthof',
    'link' => 'bt_hof.php',
    'admin_only' => '0',
  ),
  'QuiMSonde' => 
  array (
    'action' => 'QuiMSonde',
    'menu' => 'QuiMSonde',
    'root' => 'quimsonde',
    'link' => 'QuiMSonde.php',
    'admin_only' => '0',
  ),
  'leslunes' => 
  array (
    'action' => 'leslunes',
    'menu' => 'Tout sur<br>les lunes',
    'root' => 'leslunes',
    'link' => 'leslunes.php',
    'admin_only' => '0',
  ),
  'inactifs' => 
  array (
    'action' => 'inactifs',
    'menu' => 'Analyse Inactifs',
    'root' => 'inactifs',
    'link' => 'index.php',
    'admin_only' => '0',
  ),
  'tempsvols' => 
  array (
    'action' => 'tempsvols',
    'menu' => 'Temps de vol',
    'root' => 'tempsvols',
    'link' => 'tempsvols.php',
    'admin_only' => '0',
  ),
  'sign' => 
  array (
    'action' => 'sign',
    'menu' => 'sign',
    'root' => 'sign',
    'link' => 'index.php',
    'admin_only' => '0',
  ),
  'gestion' => 
  array (
    'action' => 'gestion',
    'menu' => 'Gestion MOD',
    'root' => 'gestionmod',
    'link' => 'gestion.php',
    'admin_only' => '1',
  ),
  'hof' => 
  array (
    'action' => 'hof',
    'menu' => 'Hall of Fame',
    'root' => 'hof',
    'link' => 'hof.php',
    'admin_only' => '0',
  ),
);

?>