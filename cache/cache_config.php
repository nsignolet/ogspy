<?php

define('OGSPY_CONFIG_LOADED', 1);

$server_config = array (
  'allied' => '',
  'ally_protection' => '',
  'astro_strict' => '0',
  'block_ratio' => '0',
  'color_ally' => 'Magenta_Yellow_Red',
  'config_cache' => '3600',
  'ddr' => '0',
  'debug_log' => '0',
  'default_skin' => 'skin/OGSpy_skin/',
  'disable_ip_check' => '1',
  'enable_members_view' => '0',
  'enable_register_view' => '',
  'enable_stat_view' => '1',
  'galaxy_by_line_ally' => '7',
  'galaxy_by_line_stat' => '7',
  'keeprank_criterion' => 'day',
  'last_maintenance_action' => '1535320800',
  'log_phperror' => '0',
  'max_battlereport' => '10',
  'max_favorites' => '20',
  'max_favorites_spy' => '10',
  'max_keeplog' => '7',
  'max_keeprank' => '90',
  'max_keepspyreport' => '90',
  'max_spyreport' => '10',
  'mod_cache' => '604800',
  'nb_colonnes_ally' => '3',
  'num_of_galaxies' => '7',
  'num_of_systems' => '499',
  'open_admin' => '------',
  'open_user' => '------',
  'portee_missil' => '1',
  'ratio_limit' => '0',
  'reason' => '',
  'register_alliance' => '',
  'register_forum' => '',
  'servername' => 'Cartographie',
  'server_active' => '1',
  'session_time' => '30',
  'speed_uni' => '1',
  'system_by_line_ally' => '10',
  'system_by_line_stat' => '10',
  'uni_arrondi_galaxy' => '1',
  'uni_arrondi_system' => '1',
  'url_forum' => 'http://wwfogameteam.forumcanada.net/',
  'version' => '3.3.2',
  'xtense_allow_connections' => '1',
  'xtense_log_ally_list' => '1',
  'xtense_log_empire' => '0',
  'xtense_log_messages' => '1',
  'xtense_log_ranking' => '1',
  'xtense_log_reverse' => '0',
  'xtense_log_spy' => '1',
  'xtense_log_system' => '1',
  'xtense_plugin_root' => '0',
  'xtense_spy_autodelete' => '1',
  'xtense_strict_admin' => '0',
  'xtense_universe' => 'https://s140-fr.ogame.gameforge.com',
);

?>